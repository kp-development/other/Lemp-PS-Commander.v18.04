#!/bin/bash

if [ "$(whoami)" != "root" ]; then
	echo "Only root can do this.";
	exit 1;
else

	DWIDTH=40
	DHEIGHT=13
	DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"; 

	# Silently install dialog
	apt update >/dev/null 2>&1
	apt -y install dialog >/dev/null 2>&1
	
	cmd=(dialog --clear --backtitle "Get You Online Commander" --title "Get You Online Commander" --menu "What would you like to do?" $DHEIGHT $DWIDTH 6)

	options=(0 "Install/Update the Commander"
			 1 "Manage Sites"
			 2 "Manage Accounts"
			 3 "Manage Databases"
			 4 "Manage the Server"
			 99 "EXIT")

	while true; do

		choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
		
		# If cancelled, drop the dialog
		if [ $? -ne 0 ]; then
			dialog --clear --title "Exit Commander" --infobox "We'll see you next time." 0 0
			sleep 2;
			clear;
			exit;
		fi;
		
		for choice in $choices; do
			case $choice in
				0)
					#install/update GYO Commander
					dialog --keep-tite --clear --title "Install/Update the Commander" \
						--yesno "This may not be unattendend.\nAre you sure you want to do this?" 0 0
					if [ $? -eq 0 ]; then
						#yes
						dialog --title "Install/Update the Commander" --infobox "The Commander will close once this completes.\n\nPlease restart your server when it completes." 0 0
						sleep 3;
						git pull; #in case there are any newer updates
						clear;
						source usr/installer;
						echo "Install/Update completed.  Exitting...";
						echo "Please make sure to reboot your server.";
						exit 1;
					else
						break;
					fi;
					;; 
				1) 
					# manage sites
					source usr/mansite; # include the code below in this
					;;
				2)
					# manage accounts
					source usr/manaccount; # include the code below in this
					;;
				3)
					# manage databases
					source usr/mandata; # include the code below in this
					;;
				4)
					# manage the server
					source usr/manserver; # include the code below in this
					;;
				99|*)
					dialog --title "Exit Commander" --infobox "We'll see you next time." 7 $DWIDTH
					sleep 2;
					clear;
					exit;
					;;
			esac		
		done
	done
fi;
	