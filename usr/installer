#!/bin/bash
# Installer

INSTALL_LOG=~/lemp-commander-install-log.txt
# get rid of it if it already exists...  we want a fresh one each time this runs...
rm -f $INSTALL_LOG

if [ "$(whoami)" != "root" ]; then
	echo "Only root can do this.";
	exit 1;
else
	#Random PW Generator
	random-string()
	{
		cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-32} | head -n 1
	}

	# We will need this scripts folder
	#DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )";
	clear;
	echo '--------------------------------------------------------------------';
	echo '--------------------------------------------------------------------';
	echo ' ';
	echo 'Welcome to the LEMP Commander Installer';
	echo 'This installer will install or update and configure most items needed';
	echo 'for managing your web server.';
	sleep 2;
	echo ' ';
	echo 'Please pay attention, this may not be an un-attended install';
	echo 'Please see the README for our recommended initial server setup';
	echo 'Have some patience, this can take awhile...';
	sleep 2;
	echo ' ';
	echo '--------------------------------------------------------------------';
	echo '--------------------------------------------------------------------';
	echo 'Let us begin...';
	sleep 2;
	
	# Install the needed server software
	echo '--------------------------------------------------------------------';
	echo '--------------------------------------------------------------------';
	echo 'Checking for, and installing the necessary server software';
	echo 'If already installed, we will update now';
	
	# Configure UFW
	ufw allow http >> $INSTALL_LOG
	ufw allow https >> $INSTALL_LOG
	ufw allow 2269 >> $INSTALL_LOG
	ufw --force enable >> $INSTALL_LOG
	
	# Do an apt update long before we start installing anything...
	apt-get update >> $INSTALL_LOG
	apt-get -y upgrade >> $INSTALL_LOG
	apt-get -y dist-upgrade >> $INSTALL_LOG
	apt-get -y autoremove >> $INSTALL_LOG
	
	# Force us to EST timezone
	timedatectl set-timezone America/New_York >> $INSTALL_LOG
	
	# Install a couple image processors
	apt-get -y install optipng jpegoptim curl >> $INSTALL_LOG
	
	# Install zip/unzip, lzop, time server, and git - if needed
	apt-get -y install zip unzip lzop ntp ntpdate git >> $INSTALL_LOG

	# Install python
	apt-get -y install python >> $INSTALL_LOG
	
	# Install nginx with pagespeed & mod_security modules
	apt-get -y install g++ flex bison curl doxygen libyajl-dev libgeoip-dev libtool dh-autoreconf libxml2 libpcre++-dev libxml2-dev libcurl4-openssl-dev libtool m4 automake autoconf pkgconf libxslt1.1 libxml2 libxslt-dev libxml2-dev libgd-dev libgeoip-dev openssl libssl-dev libperl-dev libpcre3 libpcre3-dev libpam-dev >> $INSTALL_LOG

	# echo "Installing Stock nGinx with PageSpeed Modules";
	# There's an issue compiling the 'latest' version because it does not match pagespeeds latest version, so let's grab the latest stable version number
	# latest version compiles properly for now
#	tVer=$(curl -s 'http://nginx.org/en/download.html' | sed 's/</\'$'\n''</g' | sed -n '/>Stable version$/,$ p' | egrep -m1 -o '/download/nginx-.+\.tar\.gz')
#	fVer=${tVer##*-}
#	Ver=${fVer%.tar.gz}
	
	# Compile LibModsecurity
	rm -rf /opt/ModSecurity*
	cd /opt/
	git clone https://github.com/SpiderLabs/ModSecurity.git >> $INSTALL_LOG
	cd ModSecurity
	git checkout -b v3/master origin/v3/master >> $INSTALL_LOG
	./build.sh >> $INSTALL_LOG
	git submodule init >> $INSTALL_LOG
	git submodule update >> $INSTALL_LOG
	./configure >> $INSTALL_LOG
	make >> $INSTALL_LOG
	make install >> $INSTALL_LOG
	
	# Grab the nginx connector
	cd /opt/ 
	git clone https://github.com/SpiderLabs/ModSecurity-nginx.git >> $INSTALL_LOG
	
	# Compile in Brotli Support
    rm -rf /usr/local/src/ngx_brotli;
	cd /usr/local/src >> $INSTALL_LOG
	git clone https://github.com/google/ngx_brotli.git >> $INSTALL_LOG
	cd ngx_brotli >> $INSTALL_LOG
	git submodule update --init --recursive >> $INSTALL_LOG
	
	# Now compile nginx with the modsecurity connector module included
	cd $DIR
	bash <(curl -k -f -L -sS https://ngxpagespeed.com/install) --assume-yes --nginx-version latest -a "--prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/media/cache/nginx/client_temp --http-proxy-temp-path=/media/cache/nginx/proxy_temp --http-fastcgi-temp-path=/media/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/media/cache/nginx/uwsgi_temp --http-scgi-temp-path=/media/cache/nginx/scgi_temp --user=www-data --group=www-data --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-mail --with-mail_ssl_module --with-http_xslt_module --with-http_image_filter_module --with-stream --with-stream_ssl_module --with-http_perl_module --add-module=/opt/ModSecurity-nginx --add-module=/usr/local/src/ngx_brotli" >> $INSTALL_LOG
	
	# Copy over the proper mod_sec files
	cp /opt/ModSecurity/modsecurity.conf-recommended /etc/nginx/modsecurity.conf >> $INSTALL_LOG
	sed -i "s/SecRuleEngine DetectionOnly/SecRuleEngine On/" /etc/nginx/modsecurity.conf >> $INSTALL_LOG
	rm -rf /etc/nginx/owasp-modsecurity-crs >> $INSTALL_LOG
	cd /etc/nginx
	git clone --single-branch -b v3.0/master https://github.com/SpiderLabs/owasp-modsecurity-crs.git >> $INSTALL_LOG
	
	cd owasp-modsecurity-crs/rules >> $INSTALL_LOG
	mv REQUEST-900-EXCLUSION-RULES-BEFORE-CRS.conf.example REQUEST-900-EXCLUSION-RULES-BEFORE-CRS.conf >> $INSTALL_LOG
	mv RESPONSE-999-EXCLUSION-RULES-AFTER-CRS.conf.example RESPONSE-999-EXCLUSION-RULES-AFTER-CRS.conf >> $INSTALL_LOG
	# Whitelist some Wordpress actions
	echo "SecRuleRemoveById 340151 340153 1234234 300015 300016 300017 950907 950005 950006 960008 960011 960904 959006 980130 200004 960010 960012" > whitelist.conf
	cd $DIR
	
	# We need to hold the nginx package, so when the updater runs, it will not update nginx to stock version
	apt-mark hold nginx* >> $INSTALL_LOG
	echo nginx* hold | dpkg --set-selections >> $INSTALL_LOG
	apt-mark hold nginx >> $INSTALL_LOG
	echo nginx hold | dpkg --set-selections >> $INSTALL_LOG
	# Clean up /tmp
	rm -rf /tmp/* >> $INSTALL_LOG
	cp $DIR/etc/init.d/nginx /etc/init.d/nginx >> $INSTALL_LOG
	chmod +x /etc/init.d/nginx >> $INSTALL_LOG
	mkdir -p /media/cache/nginx/client_temp >> $INSTALL_LOG
	mkdir -p /media/cache/nginx/proxy_temp >> $INSTALL_LOG
	mkdir -p /media/cache/nginx/fastcgi_temp >> $INSTALL_LOG
	mkdir -p /media/cache/nginx/uwsgi_temp >> $INSTALL_LOG
	mkdir -p /media/cache/nginx/scgi_temp >> $INSTALL_LOG
	mkdir -p /media/cache/ngx_pagespeed >> $INSTALL_LOG
	cp $DIR/usr/nginx.service /lib/systemd/system/ >> $INSTALL_LOG
	systemctl daemon-reload >> $INSTALL_LOG
		
	# Install memcached - if needed
	apt-get -y install memcached >> $INSTALL_LOG
	
	# Install php 7 along with some needed modules - if needed
	apt-get -y install php7.2-fpm php7.2-curl php7.2-gd php7.2-intl php7.2-mysql php7.2-json php7.2-sqlite3 php7.2-opcache php-memcached php-pear php7.2-mbstring php7.2-cli php7.2-zip php7.2-soap >> $INSTALL_LOG
	
	# Install Monit
	apt-get -y install monit >> $INSTALL_LOG
	
	# We need these for ab and htpasswd
	apt-get -y install apache2-utils >> $INSTALL_LOG
	
	# install maldet commented out for now.
	if [ -f /usr/local/maldetect/maldet ]; then
		# its installed let's skip it
		echo '';
	else
		apt-get -y install inotify-tools >> $INSTALL_LOG
		cd /usr/local/src/
		wget http://www.rfxn.com/downloads/maldetect-current.tar.gz >> $INSTALL_LOG
		mkdir maldetect-current
		tar -xzf maldetect-current.tar.gz -C maldetect-current >> $INSTALL_LOG
		cd maldetect-current
		cd maldetect-*
		sh ./install.sh >> $INSTALL_LOG
		cp $DIR/usr/local/maldetect/monitor_paths /usr/local/maldetect/ >> $INSTALL_LOG
		chmod 644 /usr/local/maldetect/monitor_paths >> $INSTALL_LOG
	fi;
	
	# install pure-ftpd
	apt-get -y install pure-ftpd >> $INSTALL_LOG
	
	# Let's check if have postfix and we need to configure it
	debconf-set-selections <<< "postfix postfix/mailname string web" >> $INSTALL_LOG
	debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Satelite'" >> $INSTALL_LOG
	apt-get install -y postfix mailutils >> $INSTALL_LOG
	
	# Install fail2ban as well
	apt-get -y install fail2ban >> $INSTALL_LOG
	echo ' ';
	
	# Install Let's Encryp Client
	apt-get install -y letsencrypt >> $INSTALL_LOG
	# if the dhparam cert doesnt exist, generate it now
	if [ ! -f /etc/ssl/certs/dhparam.pem ]; then
		openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048 >> $INSTALL_LOG
	fi;
	
	# Now install mysql, if necessary...   we'll do a check here, we won't want to run mysql_secure_installation if it's already installed
	if [ -f /usr/bin/mysql ]; then
		# its installed let's skip it
		echo '';
	else
		TMPRPW=$(random-string 15);		
		
		apt-get -y install mariadb-server >> $INSTALL_LOG
		
		# Creating Administrative Task User
		AUUName=$(random-string 5)"_admin_user";	
		AUPW=$(random-string 20);
		mysql -u root -p"$TMPRPW" -e "CREATE USER '$AUUName'@'localhost' IDENTIFIED BY '$AUPW'" >> $INSTALL_LOG
		mysql -u root -p"$TMPRPW" -e "GRANT ALL PRIVILEGES ON *.* TO '$AUUName'@'localhost' WITH GRANT OPTION" >> $INSTALL_LOG
		mysql -u root -p"$TMPRPW" -e "FLUSH PRIVILEGES" >> $INSTALL_LOG
		
		echo 'Your default root password has been set to a random password.';
		echo "Please take note of it now: $TMPRPW";
		sleep 3;
		echo 'We recommend running the secure install... It will create an administrative user with '
		echo 'full privileges, and remove the insecure "root" account.';
		echo "We have also created another secure user named $AUUName ";
		echo "for administrative tasks (backups, account creation, etc...)";
		echo "If you update the password for this user, please also change it in the following files: ";
		echo '/usr/bin/backup';
		echo '/usr/bin/account-backup';
		echo '/usr/bin/new-site';
		echo ' ';
		echo ' ';
		
		# Let's figure out if we should secure this install...
		echo 'Do you want to secure your install? (y|n)';
		read SQLSecureInstall;
		if [ $SQLSecureInstall == 'y' ]; then
			echo '--------------------------------------------------------------------';
			# Running tasks for mysql_secure_installation
			mysql -u root -p"$TMPRPW" -e "DELETE FROM mysql.user WHERE User=''" >> $INSTALL_LOG
			mysql -u root -p"$TMPRPW" -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%'" >> $INSTALL_LOG
			mysql -u root -p"$TMPRPW" -e "FLUSH PRIVILEGES" >> $INSTALL_LOG
			# Creating new Master Admin Account with full privileges
			echo 'Please enter your new Master Admin Username: (DO NOT USE root)';
			read MAUName;
			echo 'Please enter your new Master Admin Password: ';
			echo '(we will not present this back to you, please remember it)';
			read MAPWord;
			mysql -u root -p"$TMPRPW" -e "CREATE USER '$MAUName'@'localhost' IDENTIFIED BY '$MAPWord'" >> $INSTALL_LOG
			mysql -u root -p"$TMPRPW" -e "GRANT ALL PRIVILEGES ON *.* TO '$MAUName'@'localhost' WITH GRANT OPTION" >> $INSTALL_LOG
			mysql -u root -p"$TMPRPW" -e "FLUSH PRIVILEGES" >> $INSTALL_LOG
			# Removing ROOT user account
			mysql -u $AUUName -p"$AUPW" -e "DROP USER 'root'@'localhost'" >> $INSTALL_LOG
			mysql -u $AUUName -p"$AUPW" -e "FLUSH PRIVILEGES" >> $INSTALL_LOG
			echo 'MySQL is now secured';
			echo '--------------------------------------------------------------------';
		fi;
		echo 'MySQL is all set, continuing...';
		
		# We will need these later on the server...  uggggg.  make them read-only
		mkdir -p /usr/bin/templates/;
		echo $AUUName > /usr/bin/templates/.mucnf;
		echo $AUPW > /usr/bin/templates/.mpcnf;
		chmod 400 /usr/bin/templates/.mucnf;
		chmod 400 /usr/bin/templates/.mpcnf;
	fi;
	
	echo 'Server Software Install Complete'
	echo '--------------------------------------------------------------------';
	echo '--------------------------------------------------------------------';
	sleep 2;
	
	echo 'Now we will work on installing everything needed for the Commander';
	
	# Copy in our configurations and binaries
	cd $DIR;
	chmod +x /etc/init.d/nginx >> $INSTALL_LOG
	# let's stop our services first
	/etc/init.d/mysql stop >> $INSTALL_LOG
	/etc/init.d/memcached stop >> $INSTALL_LOG
	/etc/init.d/php7.2-fpm stop >> $INSTALL_LOG
	/etc/init.d/nginx stop >> $INSTALL_LOG
	/etc/init.d/pure-ftpd stop >> $INSTALL_LOG
	/etc/init.d/postfix stop >> $INSTALL_LOG
	/etc/init.d/monit stop >> $INSTALL_LOG
	
	cp -a etc/* /etc >> $INSTALL_LOG
	chmod +x /etc/init.d/nginx >> $INSTALL_LOG
	chown -R www-data:www-data /etc/nginx/default-site >> $INSTALL_LOG
	# get amount of system RAM.  We'll need this to reconfigure MySQL, PHP OPCache, and Memcached
	MEM=$(free -m | grep -oP '\d+' | head -n 1);
	if [ $MEM -gt 16384 ]; then MEM=16384; fi;
	BUFFERS=16
	if [ $MEM -lt 4096 ]; then
		BUFFERS=8
	fi;
	if [ $MEM -lt 1024 ]; then
		BUFFERS=4
	fi;
	
	# Update Memcached config
	FORMEMCACHE=$(($MEM / 16)); # Gives us optimal value for memory usage, we can also use this vaiable for other items # we can use this for the ramdisk if on our server(s)
	FOROTHERMEM=$(($MEM / 32));
	sed -i "s/##MEMCACHE##/$FORMEMCACHE/g" /etc/memcached.conf >> $INSTALL_LOG
	
	# Update MySQL Config
	sed -i "s/##MEMCACHE##/$FOROTHERMEM/g" /etc/mysql/mariadb.conf.d/50-server.cnf >> $INSTALL_LOG
	if [ -z "$AUUName" ]; then
		AUUName=$(cat /usr/bin/templates/.mucnf);
	fi;
	if [ -z "$AUPW" ]; then
		AUPW=$(cat /usr/bin/templates/.mpcnf);
	fi;
	sed -i "s/##MUN##/$AUUName/g" /etc/mysql/debian.cnf >> $INSTALL_LOG
	sed -i "s/##MPW##/$AUPW/g" /etc/mysql/debian.cnf >> $INSTALL_LOG

	# Update OPCache Config
	sed -i "s/##MEMCACHE##/$FOROTHERMEM/g" /etc/php/7.2/fpm/php.ini >> $INSTALL_LOG
	sed -i "s/##BUFFERS##/$BUFFERS/g" /etc/php/7.2/fpm/php.ini >> $INSTALL_LOG
	
	# Update Pure-FTPD Passive IP Address
	EXTIP=$(curl -s http://whatismyip.akamai.com/);
	sed -i "s/##EXTERNALIP##/$EXTIP/g" /etc/pure-ftpd/conf/ForcePassiveIP >> $INSTALL_LOG
	
	# install our sysctl.conf
	sysctl -p >> $INSTALL_LOG
	
	# install our default cron jobs
	hn=$(hostname)
	if [ $hn = 'getyou' ]; then
		crontab usr/mine.cron >> $INSTALL_LOG
	else
		crontab usr/gyo.cron >> $INSTALL_LOG
	fi;
	
	# Now restart our services
	/etc/init.d/mysql restart >> $INSTALL_LOG
	/etc/init.d/memcached restart >> $INSTALL_LOG
	/etc/init.d/php7.2-fpm restart >> $INSTALL_LOG
	/etc/init.d/nginx restart >> $INSTALL_LOG
	/etc/init.d/pure-ftpd restart >> $INSTALL_LOG
	/etc/init.d/postfix restart >> $INSTALL_LOG
	/etc/init.d/monit restart >> $INSTALL_LOG
	/etc/init.d/cron restart >> $INSTALL_LOG
	
	# Install our binaries
	cp -a usr/* /usr >> $INSTALL_LOG
	
	# Now make them executable  
	for F in usr/bin/*
	do
		chmod +x /$F >> $INSTALL_LOG
	done;

	# Now install phpmyadmin, only if we need to
	if [ ! -d /usr/share/phpmyadmin ]; then 
		sleep 2;
		echo 'phpmyadmin phpmyadmin/dbconfig-install boolean false' | debconf-set-selections
		echo 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect none' | debconf-set-selections
		apt-get -y --no-install-recommends install dbconfig-common dbconfig-mysql javascript-common libaprutil1-dbd-sqlite3 libaprutil1-ldap libjs-jquery libjs-sphinxdoc libjs-underscore liblua5.1-0 libmcrypt4 php-gettext php-phpseclib php-tcpdf phpmyadmin >> $INSTALL_LOG
		# let's make sure the web server can read the site...
		chown -R www-data:www-data /usr/share/phpmyadmin >> $INSTALL_LOG
		rm -rf /lib/systemd/system/apache* >> $INSTALL_LOG
		rm -f /etc/init.d/apache* >> $INSTALL_LOG
		systemctl daemon-reload >> $INSTALL_LOG
		# let's make sure the web server can read the site...
		chown -R www-data:www-data /usr/share/phpmyadmin >> $INSTALL_LOG
	fi;
	echo ' ';
	
	# Let's create a cache folder... on my server this is setup to be a ramdisk
	if [ ! -d /media/cache ]; then 
		mkdir -p /media/cache >> $INSTALL_LOG
		# Let's see if /media/cache is mounted or not
		if mount | grep /media/cache > /dev/null; then
			echo "Ramdisk Mounted... skipping..."
		else
			# not mounted, but let's double check fstab incase we already set this update
			T=$(grep -ir "/media/cache" /etc/fstab);
			if [ ! -n $T ]; then
				echo "tmpfs   /media/cache    tmpfs   size=$FORMEMCACHEm,mode=0777,noatime    0       0" >> /etc/fstab
				mount -a >> $INSTALL_LOG
			fi;
		fi;
		
	fi;
	
	echo '--------------------------------------------------------------------';
	echo "Now we will proceed installing our backup system"
	echo "You will need your Access Key Secret Key, and Region from Amazons S3 service"
	sleep 2
	apt-get install -y s3cmd awscli jq python-pip >> $INSTALL_LOG
	pip install boto3 >> $INSTALL_LOG
	s3cmd --configure
	aws configure
	
	sleep 2;
	echo '--------------------------------------------------------------------';
	echo '--------------------------------------------------------------------';
	echo 'You are good to go!  Welcome to LEMP Commander!';
	echo 'Please check the README for usage.';
	sleep 2;
	echo '--------------------------------------------------------------------';
	echo '--------------------------------------------------------------------';
	echo 'It is highly recommended that you restart your server now.';
	echo '--------------------------------------------------------------------';
	echo '--------------------------------------------------------------------';
	
fi;
